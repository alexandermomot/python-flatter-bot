#!/usr/bin/env python3
# coding: utf8

from pymysql.cursors import Cursor as cursor
import pymysql
import time
import logging
from datetime import datetime


class Connector:

    def __init__(self):
        self.db_name = 'flatter'
        self.ip = '127.0.0.1'
        self.port = 3306
        self.user = 'root'
        self.time_format = '%Y-%m-%d %H:%M:%S'
        self.getlist = lambda records: [int(x[0]) for x in records]
        self.getlist2 = lambda records: [{'id': int(x[0]), 'url': str(x[1])} for x in records]
        self.getlist_active = lambda records: [{'id': int(x[0]), 'active': int(x[2])} for x in records]
        self.unixtime = lambda: int(time.time())
        self.readable_time = lambda x: datetime.fromtimestamp(x).strftime(self.time_format)
        # self.connection = pymysql.connect(
        #     host=self.ip,
        #     user=self.user,
        #     password='root',
        #     database=self.db_name,
        #     port=3306,
        #     charset='utf8'
        # )
        # self.connection.ping()
        logging.info('Connected to DB with ip: %s:%s, as %s' % (self.ip, self.port, self.user))

    def locals_get_last_timestamp(self):
        sql = "SELECT DISTINCT unixtime FROM flatter.thelocals_ru ORDER BY unixtime desc"
        with self.connection.cursor() as cursor:
            #columns = cursor.description
            #result = [{columns[index][0]: column for index, column in enumerate(value)} for value in cursor.fetchall()]
            #http://stackoverflow.com/questions/5010042/mysql-get-column-name-or-alias-from-query
            cursor.execute(sql)
            timestamps = self.getlist(cursor.fetchall())
        timestamps.sort()
        if not timestamps:
            last_fetch_time = 0
        else:
            last_fetch_time = max(timestamps)
        logging.debug('Getting last timestamp: %s' % self.readable_time(last_fetch_time))
        return last_fetch_time

    def locals_get_flats(self, from_time: int):
        sql = "SELECT id, url_path FROM flatter.thelocals_ru WHERE unixtime > %s" % from_time
        with self.connection.cursor() as cursor:
            cursor.execute(sql)
            flat_info = self.getlist2(cursor.fetchall())
        return flat_info

    def locals_insert_flats(self, data):
        logging.debug('Inserting %s flats to DB' % len(data))
        data = self.generate_tuple(data)
        sql = """
                INSERT INTO flatter.thelocals_ru
                (`id`, `title`, `price_rub`, `rooms`, `url_path`, `metro_distance_meters`,
                `latitude`, `longitude`, `unixtime`, `address`, `thumb`)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                ON DUPLICATE KEY UPDATE title=title, price_rub=price_rub, rooms=rooms, url_path=url_path,
                metro_distance_meters=metro_distance_meters, latitude=latitude, longitude=longitude,
                address=address, thumb=thumb
              """
        with self.connection.cursor() as cursor:
            cursor.executemany(sql, data)
        self.connection.commit()

    def subscriptions_get_users(self, only_active=True):
        sql = 'SELECT * FROM flatter.subscriptions'
        if only_active:
            sql += ' WHERE active = 1'
        with self.connection.cursor() as cursor:
            cursor.execute(sql)
            if only_active:
                users = self.getlist(cursor.fetchall())
            else:
                users = self.getlist_active(cursor.fetchall())
        return users

    def subscriptions_update_user(self, user_id: int, active: int):
        logging.debug('Change user %s subscription to %s' % (user_id, active))
        data = (active, user_id)
        sql = 'UPDATE flatter.subscriptions SET active = %s WHERE userid = %s' % data
        with self.connection.cursor() as cursor:
            cursor.execute(sql)
        self.connection.commit()

    def subscriptions_add_user(self, user_id: int, user_name: str):
        logging.debug('Adding new user %s [id:%s] to the subscriptions list' % (user_id, user_name))
        data = (user_id, user_name, 1)
        sql = 'INSERT INTO flatter.subscriptions (`userid`,`username`,`active`) VALUES (%s, %s, %s)' % data
        with self.connection.cursor() as cursor:
            cursor.execute(sql)
        self.connection.commit()

    def locals_clean(self, preserve_days: int):
        time_from = self.unixtime() - (preserve_days * 86400)
        logging.debug('Performing clean-up to flats older %s' % self.readable_time(time_from))
        sql = 'DELETE FROM flatter.thelocals_ru WHERE unixtime < %s' % time_from
        with self.connection.cursor() as cursor:
            cursor.execute(sql)
        self.connection.commit()

    def generate_tuple(self, flats):
        a = []
        for flat_id in flats:
            flat = flats[flat_id]
            a.append((
                flat['id'],
                flat['title'],
                flat['price'],
                flat['rooms'],
                flat['path'],
                0,  # metro distance meters - still in progress
                flat['latitude'],
                flat['longitude'],
                self.unixtime(),
                flat['address'],
                flat['image']
            ))
        return a

    def close(self):
        self.connection.close()
