# coding: utf8

import random
from argparse import ArgumentParser
from configparser import ConfigParser


class Configurator:

    def __init__(self):
        self.parser = ArgumentParser()
        self.parser.add_argument('--nosleep', action='store_true')
        self.config = ConfigParser()
        self.config.read('agents.ini')

    def get_arguments(self):
        return self.parser.parse_args()

    def get_random_useragent(self) -> str:
        return self.config['useragents']['useragent_{}'.format(random.randint(1, len(self.config['useragents'])))]

    def no_sleep(self):
        return self.get_arguments().nosleep