#!/usr/bin/env python3
# coding: utf8

import requests
from parsers.config import Configurator
from lxml.etree import ElementTree
import html5lib


class Provider:

    session = None

    def __init__(self, site):
        self.session = requests.Session()
        self.session.trust_env = False  # To disable possible system proxies (for now)
        self.session.headers['User-Agent'] = Configurator().get_random_useragent()
        self.session.get(site)  # Getting basic start cookies

    def request(self, url) -> requests.Response:
        return self.session.get(url)

    @staticmethod
    def dom(string: str) -> ElementTree:
        """
        :param string: raw string to parse
        :return: etree
        """
        return html5lib.parse(string, treebuilder='lxml', namespaceHTMLElements=False)
