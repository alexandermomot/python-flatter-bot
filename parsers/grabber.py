#!/usr/bin/env python3
# coding: utf8

import logging
import random
from datetime import datetime
from parsers.thelocals import TheLocalsProvider
from db.point import Connector
from notifiers.telegram import TelegramNotifier


def get_human_time(unix_time: int, fmt: str = '%Y-%m-%d %H:%M:%S'):
    return datetime.fromtimestamp(unix_time).strftime(fmt)


def grabber():
    logging.info('Begin parsing flats from: %s ...' % '')

    db = Connector()
    provider = TheLocalsProvider()
    bot = TelegramNotifier()

    _before = db.locals_get_last_timestamp()
    invoke(provider)
    db.locals_insert_flats(provider.get_array_of_flats())
    _after = db.locals_get_last_timestamp()

    if _after > _before:
        logging.debug('Before: %s, after: %s. Getting flats...' % (get_human_time(_before), get_human_time(_after)))
        flats = db.locals_get_flats(_before)
        users = db.subscriptions_get_users()
        bot.send_flats(users, flats)
        logging.info('New flats found: %s. Id\'s: %s' % (len(flats), str(flats)))

    db.locals_clean(180)  # now, let's cleanup old data


def invoke(provider: TheLocalsProvider):
    logging.info('Invoking "' + provider.__class__.__name__ + '" provider')
    provider.set_filters(rooms=[1, 2], price_max=50000)
    provider.fetch_flats(random.randint(2, 4))
    provider.fetch_and_map_coordinates()

if __name__ == '__main__':
    logging.basicConfig(
        format='[%(asctime)s] %(levelname)s: [%(module)s] [%(funcName)s]: %(message)s',
        level=logging.DEBUG
    )
    grabber()
