#!/usr/bin/env python3
# coding: utf8

import random
import time
import logging
from parsers.provider import Provider

# TODO: create universal params generator : def filters(self, **kwargs):
# TODO: make iterable :
# TODO: "".join(['&filter[{caption}][]={value}'.format(caption='rooms', value=i) for x in range(1,3)])
# TODO: custom filter via **kwargs
# TODO: flat_caption - make it lazy (with check to identity)


class TheLocalsProvider(Provider):

    base = 'https://thelocals.ru'
    api_endpoint = '/api/frontend/rooms'
    gps_endpoint = '/api/frontend/rooms/map'
    container_xpath = "//div[@class='container']//div[@class='main-card-desc']/p"
    params = 'filter[kind]=apartment&filter[rooms][]=1&filter[rooms][]=2&filter[rooms][]=3&filter[price_max]=50000'

    def __init__(self):
        super().__init__(self.base)
        self.flats_url = self.base + self.api_endpoint
        self.coordinates_url = self.base + self.gps_endpoint
        self.flat_array = {}
        self.coordinates_fetched = False
        time.sleep(random.randint(4, 15))   # in order to be quiet

    def fetch_flats(self, pages: int = 3):
        """
        Забирает массив квартир с сайта по кол-ву страниц (страница = 1 запрос). Записывает в массив flat_array
        :param pages: количество страниц, с которых нужно забрать квартиры, начиная с первой и до pages
        """
        logging.debug('Begin fetching flats...')
        pages += 1
        flats_pages = []
        for i in range(1, pages):
            response = self.request(self.flats_url + "?" + self.params + "&page=%s" % i)
            flats_pages += [response.json()['ads']]
            time.sleep(random.randint(2, 8))  # in order to be quiet
        for page in flats_pages:
            for flat in page:
                flat['price'] = int("".join(flat['price'].split()))  # remove all spaces in price digits
                if flat['id'] not in self.flat_array:
                    self.flat_array[flat['id']] = flat
                self.flat_array[flat['id']].update(flat)
        logging.debug('Flats fetched: %s' % len(self.flat_array))

    def fetch_and_map_coordinates(self):
        """
        Забрать массив координат и смаппить на текущие квартиры, взятые через fetch_flats()
        итоговый массив в flat_array
        """
        logging.debug('Begin fetching GPS coordinates and mapping...')
        response = self.request(self.coordinates_url)
        for flat_gps in response.json()['ads']:
            data = {'latitude': flat_gps['lat'], 'longitude': flat_gps['lng'], 'address': flat_gps['address']}
            if flat_gps['id'] not in self.flat_array:
                continue
            self.flat_array[flat_gps['id']].update(data)
        self.coordinates_fetched = True
        logging.debug('Total number of flats with coordinates: %s' % len(response.json()['ads']))

    def set_filters(self, rooms: list, price_max: int = 50000, kind: str = 'apartment'):
        """
        Задать фильтры для поиска на thelocals.ru, которые являются get-параметрами
        :param rooms: комнаты, массив напр. [1,2]
        :param price_max: максимальная цена
        :param kind: тип (apartment, rooms)
        """
        self.params = 'filter[{caption}]={value}'.format(caption='kind', value=kind)
        for i in rooms:
            self.params += '&filter[{caption}][]={value}'.format(caption='rooms', value=i)
        self.params += '&filter[{caption}]={value}'.format(caption='price_max', value=price_max)

    def get_flat_caption(self, flat_id: int = 0, *args) -> str:
        descr = "id: %s \ntitle: %s \ncaption: %s"
        if 'caption' in self.flat_array[flat_id]:
            flat = self.flat_array[flat_id]
            return descr % (flat['id'], flat['title_i'], flat['caption'])
        caption = ''
        if flat_id not in self.flat_array:
            raise TheLocalsException('There is not id: %s found in the list' % flat_id)
        flat = self.flat_array[flat_id]
        if 'path' not in flat:
            raise TheLocalsException('Flat /path is not found by id: %s, should be fetched more' % flat_id)
        if flat['path'].find('/', 0, 1) == -1:
            flat['path'] = '/' + flat['path']
        raw_html = self.request(self.base + flat['path']).content.decode()
        dom = self.dom(raw_html)
        for description in dom.findall(self.container_xpath):
            if description.text is None:
                continue
            else:
                caption += description.text
        title = dom.find('//title').text
        self.flat_array[flat['id']].update({'id': flat['id'], 'title_i': title, 'caption': caption})
        return descr % (flat['id'], title, caption)

    def get_flat_coordinates(self, flat_id=0):
        if not self.coordinates_fetched:
            raise TheLocalsException('There is no coordinates fetched yet, please use fetch_coordinates method first')
        if flat_id not in self.flat_array:
            raise TheLocalsException('There is no coordinates for the flat_id: %s found in the list' % flat_id)
        flat_data = self.flat_array[flat_id]
        return {'lat': flat_data['latitude'], 'lng': flat_data['longitude'], 'addr': flat_data['address']}

    def get_random_flat_id(self):
        i = 0
        while i < 100000000:  # TODO: set do not perform if there is no paths
            random_flat_id = random.choice(list(self.flat_array.keys()))
            if 'path' in self.flat_array[random_flat_id]:
                break
            i += 1
        else:
            raise TheLocalsException('There is no flat with /path were found, please refetch')
        return self.flat_array[random_flat_id]['id']

    def get_array_of_flats(self):
        return self.flat_array


class TheLocalsException(Exception):
    pass

