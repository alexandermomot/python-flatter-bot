#!/usr/bin/env python3
# coding: utf8

import logging
from db.point import Connector as db
from twx.botapi import TelegramBot, Message, InputFileInfo, InputFile, Error


class TelegramNotifier:

    API_TOKEN = '256901471:AAHgPJ-e9ebVAOEssrSTnx2yKJWaL65C0yw'
    DOMAIN = 'https://zettachat.com'
    HOOK_URL = '/%s/hook' % API_TOKEN

    def __init__(self, ):
        logging.info('Preparing telegram bot...')
        self.user_list = None
        self.bot = TelegramBot(self.API_TOKEN)
        self.bot.update_bot_info().wait()
        self.db = db()

    def set_hook_url(self):
        logging.info('Setting the hook url...')
        #if not self.bot.set_webhook(self.DOMAIN + self.HOOK_URL).wait():
        #    raise Exception('Bot creation is unsuccessful')
        logging.info('Hook for bot %s is set' % self.bot.username)

    def get_hook_url(self):
        return self.HOOK_URL

    def get_bot(self):
        return self.bot

    def send_flats(self, user_list: list, flats: list):
        message = 'Вам - новая квартира: [%s](%s)'
        if user_list:
            self.user_list = user_list
        if not self.user_list:
            logging.info('No users found to send notifications, exit...')
            return
        logging.info('Sending messages to the users: ' + str(self.user_list))
        for user in self.user_list:
            for i, flat in enumerate(flats):
                msg = message % (flat['id'], 'https://thelocals.ru' + flat['url'])
                response = self.bot.send_message(user, msg, parse_mode='Markdown').wait()  # type: Message
                if isinstance(response, Error):
                    logging.info('User %s is unsubscribed, disabling active state. Error: %s' % (user, response.description))
                    self.db.subscriptions_update_user(user, 0)
                    break
                if i > 1:
                    break

        # fp = open('foo.png', 'rb')
        # file_info = InputFileInfo('foo.png', fp, 'image/png')
        # InputFile('photo', file_info)
        # self.bot.send_photo(chat_id=self.USER_ID, photo=InputFile)
