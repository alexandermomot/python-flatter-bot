# coding: utf8

from os.path import join, dirname, abspath
from dotenv import load_dotenv
from notifiers.telegram import TelegramNotifier


base_dir = dirname(abspath(__file__))
load_dotenv(join(base_dir, '.env'))

#bot = TelegramNotifier()
#bot.set_hook_url()
