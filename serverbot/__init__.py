#!/usr/bin/env python3
# coding: utf8

from settings import base_dir
from logging import getLevelName
from logging.handlers import RotatingFileHandler
from flask import Flask, request
from os import environ as env


class Bot(Flask):

    def __init__(self, import_name):
        super().__init__(import_name)

        log = self.logger
        handler = RotatingFileHandler(
            base_dir + '%s/%s' % (env['LOG_FOLDER'], env['LOG_FILENAME']),
            maxBytes=1048576,
            backupCount=5
        )
        handler.setFormatter(env['LOG_FORMAT'])

        log.addHandler(handler)
        log.setLevel(getLevelName(env['LOG_LEVEL']))
        log.info('Starting flatter bot web server...')

app = Bot(__name__)
from . import route

if __name__ == '__main__':
    app.run(debug=False)
