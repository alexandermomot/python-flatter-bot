# coding: utf8

from . import app
from . import request as r
log = app.logger


def log_incoming():
    headers = ''
    for header, value in r.headers:
        headers += '%s: %s\n' % (header, value)
    log.info('Address: %s\nHeaders:\n%s' % (r.remote_addr, headers))
    log.info('[%s] Args: %s' % (r.method, r.args))
    if r.method == 'POST':
        log.info('[%s] Body: \n%s' % (r.method, r.data.decode("utf-8")))
