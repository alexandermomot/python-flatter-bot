#!/usr/bin/env python3
# coding: utf8

from . import app
from . import request as r
from .helpers import log_incoming
from .model import subscribe_user
from os import environ as env
log = app.logger


@app.route('/', methods=['GET', 'POST'])
def index():
    return env['BOT_MAIN_MESSAGE']


@app.route('/test', methods=['GET', 'POST'])
#@app.route(bot.get_hook_url(), methods=['GET', 'POST'])
def hook():
    """
    Send a message to a user
    """
    log_incoming()
    if not r.is_json:
        raise Exception('JSON expected')
    try:
        message = r.json['message']
        subscribe_user(message)
    except KeyError:
        error = '[%s] Seems to be that is an error. Message body is: ' % r.method
        log.critical(error + '\n%s' % r.data.decode("utf-8"))
    return b''




