# coding: utf8

from os import environ as env
from twx.botapi import Message, Error
from db.point import Connector
# from . import bot


def subscribe_user(message):
    user_found = None
    text = 'Что-то я ничего не понял :('
    user_id = message['from']['id']
    user_name = message['from']['first_name']
    if message['text'] == '/start' and message['entities'][0]['type'] == 'bot_command':
        db = Connector()
        db_users = db.subscriptions_get_users(only_active=False)
        for db_user in db_users:
            if user_id == db_user['id']:
                user_found = db_user
                break
        else:
            text = 'Привет! Спасибо за подписку :)'
            db.subscriptions_add_user(user_id, user_name)
        if user_found:
            if user_found['active'] == 1:
                text = 'Я не могу подписать Вас дважды, вы и так уже на меня подписаны :)'
            else:
                db.subscriptions_update_user(user_id, 1)
                text = 'Привет. И добро пожаловать обратно :)'
    #bot.get_bot().send_message(user_id, text)  # type: Message
